# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160107141048) do

  create_table "action_models", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "path",                limit: 255
    t.integer  "controller_model_id", limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "action_models_user_groups", force: :cascade do |t|
    t.integer  "action_model_id", limit: 4
    t.integer  "user_group_id",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4
    t.integer  "updated_by",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "controller_models", force: :cascade do |t|
    t.string   "name",                  limit: 255
    t.string   "path",                  limit: 255
    t.string   "position",              limit: 255
    t.string   "icon",                  limit: 255
    t.text     "description",           limit: 65535
    t.string   "category",              limit: 255
    t.integer  "ordering",              limit: 4
    t.string   "controller_model_type", limit: 255
    t.integer  "created_by",            limit: 4
    t.integer  "updated_by",            limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "couriers", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4
    t.integer  "updated_by",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "departments", force: :cascade do |t|
    t.integer  "company_id",  limit: 4
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4
    t.integer  "updated_by",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "inbounds", force: :cascade do |t|
    t.string   "inbound_number",    limit: 255
    t.date     "inbound_date"
    t.time     "inbound_time"
    t.integer  "courier_id",        limit: 4
    t.string   "awb_number",        limit: 255
    t.date     "awb_date"
    t.string   "sender_name",       limit: 255
    t.string   "sender_department", limit: 255
    t.string   "sender_company",    limit: 255
    t.text     "description",       limit: 65535
    t.integer  "company_id",        limit: 4
    t.integer  "department_id",     limit: 4
    t.integer  "user_id",           limit: 4
    t.integer  "created_by",        limit: 4
    t.integer  "updated_id",        limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "package_type_id",   limit: 4
    t.integer  "inbound_status",    limit: 4
  end

  create_table "outbounds", force: :cascade do |t|
    t.string   "outbound_number",     limit: 255
    t.date     "outbound_date"
    t.time     "outbound_time"
    t.integer  "courier_id",          limit: 4
    t.string   "awb_number",          limit: 255
    t.date     "awb_date"
    t.string   "receiver_name",       limit: 255
    t.string   "receiver_department", limit: 255
    t.string   "receiver_company",    limit: 255
    t.text     "receiver_address",    limit: 65535
    t.text     "description",         limit: 65535
    t.integer  "company_id",          limit: 4
    t.integer  "department_id",       limit: 4
    t.integer  "user_id",             limit: 4
    t.integer  "created_by",          limit: 4
    t.integer  "updated_id",          limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "package_type_id",     limit: 4
    t.integer  "outbound_status",     limit: 4
  end

  create_table "package_statuses", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4
    t.integer  "updated_by",  limit: 4
    t.integer  "ordering",    limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "package_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4
    t.integer  "updated",     limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "user_groups", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.integer  "created_by",  limit: 4
    t.integer  "updated_by",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "super",       limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "created_by",             limit: 4
    t.integer  "updated_by",             limit: 4
    t.integer  "department_id",          limit: 4
    t.integer  "user_group_id",          limit: 4
    t.string   "username",               limit: 255
    t.string   "name",                   limit: 255
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.integer  "company_id",             limit: 4
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
