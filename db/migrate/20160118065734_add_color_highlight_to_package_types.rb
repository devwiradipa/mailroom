class AddColorHighlightToPackageTypes < ActiveRecord::Migration
  def change
    add_column :package_types, :color, :string
    add_column :package_types, :highlight, :string
  end
end
