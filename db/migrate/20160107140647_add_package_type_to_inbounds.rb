class AddPackageTypeToInbounds < ActiveRecord::Migration
  def change
    add_column :inbounds, :package_type_id, :integer
  end
end
