class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.integer :company_id
      t.string :name
      t.text :description
      t.integer :created_by
      t.integer :updated_by

      t.timestamps null: false
    end
  end
end
