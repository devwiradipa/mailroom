class CreateActionModelsUserGroups < ActiveRecord::Migration
  def change
    create_table :action_models_user_groups do |t|
      t.integer :action_model_id
      t.integer :user_group_id

      t.timestamps null: false
    end
  end
end
