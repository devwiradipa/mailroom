class CreateNumberingFormats < ActiveRecord::Migration
  def change
    create_table :numbering_formats do |t|
      t.integer :year
      t.string :name
      t.integer :iteration_number

      t.timestamps null: false
    end
  end
end
