class CreateInbounds < ActiveRecord::Migration
  def change
    create_table :inbounds do |t|
      t.string :inbound_number
      t.date :inbound_date
      t.time :inbound_time
      t.integer :courier_id
      t.string :awb_number
      t.date :awb_date
      t.string :sender_name
      t.string :sender_department
      t.string :sender_company
      t.text :description
      t.integer :company_id
      t.integer :department_id
      t.integer :user_id
      t.integer :created_by
      t.integer :updated_id

      t.timestamps null: false
    end
  end
end
