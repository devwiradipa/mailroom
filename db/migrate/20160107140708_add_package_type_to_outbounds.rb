class AddPackageTypeToOutbounds < ActiveRecord::Migration
  def change
    add_column :outbounds, :package_type_id, :integer
  end
end
