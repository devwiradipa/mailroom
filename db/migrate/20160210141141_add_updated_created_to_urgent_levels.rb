class AddUpdatedCreatedToUrgentLevels < ActiveRecord::Migration
  def change
    add_column :urgent_levels, :created_by, :integer
    add_column :urgent_levels, :updated_by, :integer
  end
end
