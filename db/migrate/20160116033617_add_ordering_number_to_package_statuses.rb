class AddOrderingNumberToPackageStatuses < ActiveRecord::Migration
  def change
    add_column :package_statuses, :ordering_number, :integer
  end
end
