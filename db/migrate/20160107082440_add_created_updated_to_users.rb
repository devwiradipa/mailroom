class AddCreatedUpdatedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :created_by, :integer
    add_column :users, :updated_by, :integer
    add_column :users, :department_id, :integer
    add_column :users, :user_group_id, :integer
    add_column :users, :username, :string
    add_column :users, :name, :string
  end
end
