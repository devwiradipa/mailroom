class AddLabelToUrgentLevels < ActiveRecord::Migration
  def change
    add_column :urgent_levels, :urgent_label, :string
  end
end
