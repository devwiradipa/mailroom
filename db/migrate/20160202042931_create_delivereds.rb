class CreateDelivereds < ActiveRecord::Migration
  def change
    create_table :delivereds do |t|
      t.date :delivered_date
      t.time :delivered_time
      t.string :courier_name
      t.string :receiver_name

      t.timestamps null: false
    end
  end
end
