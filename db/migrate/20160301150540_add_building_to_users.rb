class AddBuildingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :building_id, :integer
    add_column :companies, :building_id, :integer
    add_column :couriers, :building_id, :integer
    add_column :delivereds, :building_id, :integer
    add_column :departments, :building_id, :integer
    add_column :faileds, :building_id, :integer
    add_column :inbounds, :building_id, :integer
    add_column :inbound_statuses, :building_id, :integer
    add_column :internal_couriers, :building_id, :integer
    add_column :numbering_formats, :building_id, :integer
    add_column :package_statuses, :building_id, :integer
    add_column :package_types, :building_id, :integer
    add_column :urgent_levels, :building_id, :integer
    add_column :user_groups, :building_id, :integer
    add_column :with_couriers, :building_id, :integer
  end
end
