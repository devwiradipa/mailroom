class AddStatusToInbounds < ActiveRecord::Migration
  def change
    add_column :inbounds, :inbound_status, :integer
  end
end
