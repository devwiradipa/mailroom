class AddUpdatedCreatedToInternalCouriers < ActiveRecord::Migration
  def change
    add_column :internal_couriers, :created_by, :integer
    add_column :internal_couriers, :updated_by, :integer
  end
end
