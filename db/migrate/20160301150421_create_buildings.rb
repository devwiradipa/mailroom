class CreateBuildings < ActiveRecord::Migration
  def change
    create_table :buildings do |t|
      t.string :name
      t.text :description
      t.integer :created_by
      t.integer :updated_by

      t.timestamps null: false
    end
  end
end
