class CreateFaileds < ActiveRecord::Migration
  def change
    create_table :faileds do |t|
      t.date :failed_date
      t.time :failed_time
      t.string :courier_name
      t.text :reason

      t.timestamps null: false
    end
  end
end
