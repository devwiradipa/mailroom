class CreateOutbounds < ActiveRecord::Migration
  def change
    create_table :outbounds do |t|
      t.string :outbound_number
      t.date :outbound_date
      t.time :outbound_time
      t.integer :courier_id
      t.string :awb_number
      t.date :awb_date
      t.string :receiver_name
      t.string :receiver_department
      t.string :receiver_company
      t.text :receiver_address
      t.text :description
      t.integer :company_id
      t.integer :department_id
      t.integer :user_id
      t.integer :created_by
      t.integer :updated_id

      t.timestamps null: false
    end
  end
end
