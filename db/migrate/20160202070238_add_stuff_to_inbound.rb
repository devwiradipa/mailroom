class AddStuffToInbound < ActiveRecord::Migration
  def change
    add_column :inbounds, :delivered_date, :date
    add_column :inbounds, :delivered_time, :time
    add_column :inbounds, :with_courier_date, :date
    add_column :inbounds, :with_courier_time, :time
    add_column :inbounds, :failed_date, :date
    add_column :inbounds, :failed_time, :date
    add_column :inbounds, :delivered_id, :integer
    add_column :inbounds, :with_courier_id, :integer
    add_column :inbounds, :failed_id, :integer
  end
end
