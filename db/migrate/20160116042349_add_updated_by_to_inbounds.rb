class AddUpdatedByToInbounds < ActiveRecord::Migration
  def change
    add_column :inbounds, :updated_by, :integer
  end
end
