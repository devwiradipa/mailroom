class AddInternalCourierIdToDelivered < ActiveRecord::Migration
  def change
    add_column :delivereds, :internal_courier_id, :integer
    add_column :faileds, :internal_courier_id, :integer
  end
end
