class AddStatusToOutbounds < ActiveRecord::Migration
  def change
    add_column :outbounds, :outbound_status, :integer
  end
end
