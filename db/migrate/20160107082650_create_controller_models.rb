class CreateControllerModels < ActiveRecord::Migration
  def change
    create_table :controller_models do |t|
      t.string :name
      t.string :path
      t.string :position
      t.string :icon
      t.text :description
      t.string :category
      t.integer :ordering
      t.string :controller_model_type
      t.integer :created_by
      t.integer :updated_by

      t.timestamps null: false
    end
  end
end
