class CreateActionModels < ActiveRecord::Migration
  def change
    create_table :action_models do |t|
      t.string :name
      t.string :path
      t.integer :controller_model_id

      t.timestamps null: false
    end
  end
end
