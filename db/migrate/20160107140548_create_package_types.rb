class CreatePackageTypes < ActiveRecord::Migration
  def change
    create_table :package_types do |t|
      t.string :name
      t.text :description
      t.integer :created_by
      t.integer :updated

      t.timestamps null: false
    end
  end
end
