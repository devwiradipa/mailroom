class AddInternalCourierIdToWithCourier < ActiveRecord::Migration
  def change
    add_column :with_couriers, :internal_courier_id, :integer
  end
end
