class CreateInboundStatuses < ActiveRecord::Migration
  def change
    create_table :inbound_statuses do |t|
      t.integer :inbound_id
      t.integer :package_status_id
      t.integer :created_by
      t.integer :updated_by

      t.timestamps null: false
    end
  end
end
