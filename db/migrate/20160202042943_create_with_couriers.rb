class CreateWithCouriers < ActiveRecord::Migration
  def change
    create_table :with_couriers do |t|
      t.date :with_courier_date
      t.time :with_courier_time
      t.string :courier_name

      t.timestamps null: false
    end
  end
end
