class UserMailer < ApplicationMailer
	default from: "info@mailroomwiradipa.com"
  	layout 'mailer'

	def inbound_notification(inbound_id, package_type, package_description, created_by, receiver_name, receiver_email, sender_name, sender_department, sender_company, hostname)
	    @package_type = package_type
	    @package_description = package_description
	    @receiver_name = receiver_name
	    @created_by = created_by
	    @receiver_name = receiver_name
	    @sender_name = sender_name
	    @sender_department = sender_department
	    @sender_company = sender_company

	    @url  = hostname+"/inbounds/"+inbound_id.to_s
	    mail(:to => receiver_email, :subject => "Package From "+@sender_company)
  	end
end
