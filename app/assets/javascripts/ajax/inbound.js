$(function(){

	$("#inbound_company_id").change(function(){
		var companyId = parseInt($("#inbound_company_id").val());
		$.ajax({
          	type: "post",
          	data: "company_id="+companyId,
      		url: "/json/departments/get_departments",
          	success: function(response){
              	var data = JSON.parse(response);

              	var newData = "<select id='inbound_department_id' name='inbound[department_id]' class='js-example-placeholder-single js-states form-control'>";
              	newData += "<option value='0'>-- Choose Department --</option>";
              	for(var n=0;n<data.length;n++) {
	               	newData += "<option value='"+ data[n].id +"'>"+ data[n].name +"</option>";
	            }
              	newData += "</select>";

              	$("#department").empty().append(newData);
              	$(".js-example-placeholder-single").select2();

              	$("#inbound_department_id").change(function(){
					var departmentId = parseInt($("#inbound_department_id").val());
					$.ajax({
			          	type: "post",
			          	data: "department_id="+departmentId,
			      		url: "/json/users/get_users",
			          	success: function(response){
			              	var data = JSON.parse(response);

			              	var newData = "<select id='inbound_user_id' name='inbound[user_id]' class='js-example-placeholder-single js-states form-control'>";
			              	newData += "<option value='0'>-- Choose Penerima --</option>";
			              	for(var n=0;n<data.length;n++) {
				               	newData += "<option value='"+ data[n].id +"'>"+ data[n].name +"</option>";
				            }
			              	newData += "</select>";

			              	$("#user").empty().append(newData);
			              	$(".js-example-placeholder-single").select2();
			          	}
					});
				});
          	}
		});
	});

	$("#inbound_department_id").change(function(){
		var departmentId = parseInt($("#inbound_department_id").val());
		$.ajax({
          	type: "post",
          	data: "department_id="+departmentId,
      		url: "/json/users/get_users",
          	success: function(response){
              	var data = JSON.parse(response);

              	var newData = "<select id='inbound_user_id' name='inbound[user_id]' class='js-example-placeholder-single js-states form-control'>";
              	newData += "<option value'0'>-- Choose Penerima --</option>";
              	for(var n=0;n<data.length;n++) {
	               	newData += "<option value'"+ data[n].id +"'>"+ data[n].name +"</option>";
	            }
              	newData += "</select>";

              	$("#user").empty().append(newData);
              	$(".js-example-placeholder-single").select2();
          	}
		});
	});
});