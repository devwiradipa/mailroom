$(function(){

	$("#user_company_id").change(function(){
		var companyId = parseInt($("#user_company_id").val());
		$.ajax({
          	type: "post",
          	data: "company_id="+companyId,
      		url: "/json/departments/get_departments",
          	success: function(response){
              	var data = JSON.parse(response);

              	var newData = "<label for='user_department_id' class='col-sm-3 control-label'>Department</label>";
              	newData += "<div class='col-sm-7'><select id='user_department_id' name='user[department_id]' class='js-example-placeholder-single js-states form-control'>";
              	newData += "<option value='0'>-- Choose Department --</option>";
              	for(var n=0;n<data.length;n++) {
	               	newData += "<option value='"+ data[n].id +"'>"+ data[n].name +"</option>";
	            }
              	newData += "</select></div>";

              	$("#department").empty().append(newData);
                $(".js-example-placeholder-single").select2();
          	}
		});
	});
});