class ControllerModel < ActiveRecord::Base
	has_many :action_models, dependent: :destroy
end
