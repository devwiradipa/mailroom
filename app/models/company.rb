class Company < ActiveRecord::Base
	has_many :departments, dependent: :destroy
	has_many :users, dependent: :restrict_with_exception

	belongs_to :building
end
