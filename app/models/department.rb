class Department < ActiveRecord::Base
	belongs_to :company

	belongs_to :building

	has_many :users, dependent: :restrict_with_exception
end
