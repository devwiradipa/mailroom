class PackageType < ActiveRecord::Base
	has_many :inbounds, dependent: :restrict_with_exception
	has_many :outbounds, dependent: :restrict_with_exception
end
