class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable 

  belongs_to :user_group
  belongs_to :company
  belongs_to :department

  belongs_to :building

  has_many :inbounds, dependent: :restrict_with_exception

  # paperclip

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/assets/missing.png",
    :url => '/images/users/:basename_:style_:id.:extension',
    :path => ":rails_root/public/images/users/:basename_:style_:id.:extension"

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
end
