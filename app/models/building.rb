class Building < ActiveRecord::Base
	has_many :companies, dependent: :destroy
	has_many :couriers, dependent: :destroy
	has_many :delivereds, dependent: :destroy
	has_many :departments, dependent: :destroy
	has_many :faileds, dependent: :destroy
	has_many :inbounds, dependent: :destroy
	has_many :internal_couriers, dependent: :destroy
	has_many :numbering_formats, dependent: :destroy
	has_many :urgent_levels, dependent: :destroy
	has_many :users, dependent: :destroy
	has_many :user_groups, dependent: :destroy
	has_many :with_couriers, dependent: :destroy
end
