class ActionModel < ActiveRecord::Base
	belongs_to :controller_model

	has_many :action_models_user_groups, dependent: :destroy
	has_many :user_groups, through: :action_models_user_groups
end
