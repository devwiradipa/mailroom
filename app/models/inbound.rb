class Inbound < ActiveRecord::Base
	belongs_to :company
	belongs_to :user
	belongs_to :department
	belongs_to :created, class_name: "User", foreign_key: "created_by"
	belongs_to :package_type
	belongs_to :courier
	belongs_to :urgent_level
	belongs_to :with_courier
	belongs_to :delivered
	belongs_to :failed

	has_many :inbound_statuses, dependent: :destroy

	belongs_to :building
end
