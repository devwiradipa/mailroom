class UserGroup < ActiveRecord::Base
	has_many :users, dependent: :restrict_with_exception

	has_many :action_models_user_groups, dependent: :destroy
	has_many :action_models, through: :action_models_user_groups

	belongs_to :building
end
