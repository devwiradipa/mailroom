class Outbound < ActiveRecord::Base
	belongs_to :company
	belongs_to :user
	belongs_to :department
	belongs_to :created, class_name: "User", foreign_key: "created_by"
	belongs_to :package_type
end
