class UrgentLevelsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user
  before_filter :authorize_super, except: [:index]

  def index
    @urgent_levels = UrgentLevel.order("created_at DESC")
  end

  def new
    @urgent_level = UrgentLevel.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @urgent_levels }
    end
  end

  def show
    @urgent_level = UrgentLevel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @urgent_levels }
    end
  end
  
  def create
    @urgent_level = UrgentLevel.new(urgent_level_params)

    respond_to do |format|
      if @urgent_level.save        
        format.html { redirect_to(urgent_level_path(@urgent_level.id), :notice => 'UrgentLevel was successfully created.') }
        format.xml  { render :xml => @urgent_level, :status => :created, :urgent_level => @urgent_level }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @urgent_level.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @urgent_level = UrgentLevel.find(params[:id])
  end
  
  def update
    @urgent_level = UrgentLevel.find(params[:id])

    respond_to do |format|
      if @urgent_level.update_attributes(urgent_level_params)        
        format.html { redirect_to(urgent_level_path(@urgent_level), :notice => 'UrgentLevel was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @urgent_level.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @urgent_level = UrgentLevel.find(params[:id])
    @urgent_level.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'urgent_levels') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def urgent_level_params
    params.require(:urgent_level).permit(:created_by, :description, :name, :updated_by, :urgent_label)
  end
end
