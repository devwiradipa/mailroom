class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

  def index
	  if current_user.building
      @users = User.where("building_id = ?", current_user.building_id).order("created_at DESC")
    else
      @users = User.order("created_at DESC")
    end

    if current_user.building
      @all_departments = Department.where("building_id = ?", current_user.building_id).order("created_at DESC")
    else
      @all_departments = Department.order("created_at DESC")
    end

    if current_user.building
      @all_companies = Company.where("building_id = ?", current_user.building_id).order("created_at DESC")
    else
      @all_companies = Company.order("created_at DESC")
    end

    if params[:name] and params[:name] != ""
      @name = params[:name].downcase
      @users = User.where('id IN (?) AND lower(name) LIKE ? ', @users.map(&:id), '%'+ @name +'%').order('created_at DESC')
    end

    if params[:username] and params[:username] != ""
      @username = params[:username].downcase
      @users = User.where('id IN (?) AND lower(username) LIKE ? ', @users.map(&:id), '%'+ @username +'%').order('created_at DESC')
    end

    if params[:email] and params[:email] != ""
      @email = params[:email].downcase
      @users = User.where('id IN (?) AND lower(email) LIKE ? ', @users.map(&:id), '%'+ @email +'%').order('created_at DESC')
    end

    if params[:department] and params[:department] != ""
      @department = params[:department].downcase
      @departments = Department.where('lower(name) LIKE ? ', '%'+ @department +'%').order('created_at DESC')
      @users = User.where(:id => @users, :department_id => @departments)
    end

    if params[:company] and params[:company] != ""
      @company = params[:company].downcase
      @companies = Company.where('lower(name) LIKE ? ', '%'+ @company +'%').order('created_at DESC')
      @users = User.where(:id => @users, :company_id => @companies)
    end

    if params[:imei] and params[:imei] != ""
      @imei = params[:imei].downcase
      @users = User.where('id IN (?) AND lower(imei) LIKE ? ', @users.map(&:id), '%'+ @imei +'%').order('created_at DESC')
    end

    if params[:version] and params[:version] != ""
      @version = params[:version].downcase
      @users = User.where('id IN (?) AND lower(version) LIKE ? ', @users.map(&:id), '%'+ @version +'%').order('created_at DESC')
    end

    if params[:active] and params[:active] != ""
      @active = params[:active].downcase
      @users = User.where('id IN (?) AND lower(active) LIKE ? ', @users.map(&:id), '%'+ @active +'%').order('created_at DESC')
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  def new
  	@user = User.new
    if current_user.building
      @companies = current_user.building.companies
      @user_groups = current_user.building.user_groups
    else
      @companies = Company.all
      @user_groups = UserGroup.all
    end
    @buildings = Building.order("name ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user }
    end  	
  end

  def show
    @user = User.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user }
    end
  end
  
  def create
    @user = User.new(user_params)
    
    if current_user.building
      @companies = current_user.building.companies
      @user_groups = current_user.building.user_groups
    else
      @companies = Company.all
      @user_groups = UserGroup.all
    end

    @buildings = Building.order("name ASC")

    respond_to do |format|
      if @user.save       
        format.html { redirect_to(user_path(@user.id), :notice => 'User was successfully created.') }
        format.xml  { render :xml => @user, :status => :created, :user => @user }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @user = User.find(params[:id])
    
    if current_user.building
      @companies = current_user.building.companies
      @user_groups = current_user.building.user_groups
    else
      @companies = Company.all
      @user_groups = UserGroup.all
    end

    @buildings = Building.order("name ASC")
  end
  
  def update
    @user = User.find(params[:id])
    
    if current_user.building
      @companies = current_user.building.companies
      @user_groups = current_user.building.user_groups
    else
      @companies = Company.all
      @user_groups = UserGroup.all
    end
    
    @buildings = Building.order("name ASC")

    respond_to do |format|
      if @user.update_attributes(user_without_password_params)  
        format.html { redirect_to(user_path(@user.id), :notice => 'User was successfully updated.') }
      
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'users') }
      format.xml  { head :ok }
    end
  end

  def get_users
    @department = Department.find(params[:department_id].to_i)
    @users = @department.users

    respond_to do |format|
      format.js {
        render :json => @users.to_json(:except => [:updated_at]),
        :callback => params[:callback]
      }
    end
  end
  
  private
  
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :remember_me, :username, :user_group_id, :name, :avatar, :created_by, :updated_by, :department_id, :company_id, :building_id)
  end

  def user_without_password_params
    params.require(:user).permit(:email, :remember_me, :username, :user_group_id, :name, :avatar, :created_by, :updated_by, :department_id, :company_id, :building_id)
  end
end
