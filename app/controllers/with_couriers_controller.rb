class WithCouriersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

  def index
    @with_couriers = WithCourier.order("created_at DESC")
  end

  def new
    @with_courier = WithCourier.new
    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 1, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 1)
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @inbounds }
    end
  end

  def show
    @with_courier = WithCourier.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @inbounds }
    end
  end

  def create
    @with_courier = WithCourier.new(with_courier_params)
    internal_couriers = InternalCourier.find(params[:with_courier][:internal_courier_id])
    @with_courier.courier_name = internal_couriers.name

    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 1, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 1)
    end

    respond_to do |format|
      if @with_courier.save 
        id_inbounds = params[:inbounds]
        id_inbounds.each do |id|
          inbound = Inbound.find(id)
          inbound.with_courier_id = @with_courier.id
          inbound.with_courier_date = @with_courier.with_courier_date
          inbound.with_courier_time = @with_courier.with_courier_time
          inbound.inbound_status = 2
          inbound.save
        end

        format.html { redirect_to(with_couriers_path, :notice => 'Inbound was successfully created.') }
        format.xml  { render :xml => @inbound, :status => :created, :inbound => @inbound }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @inbound.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
    @with_courier = WithCourier.find(params[:id])
    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 1, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 1)
    end
  end
  
  def update
    @with_courier = WithCourier.find(params[:id])

    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 1, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 1)
    end

    inbounds = Inbound.where(with_courier_id: params[:id])
    inbounds.each do |inbound|
      inbound.with_courier_id = nil
      inbound.with_courier_date = nil
      inbound.with_courier_time = nil
      inbound.inbound_status = 1
      inbound.save
    end

    respond_to do |format|
      internal_couriers = InternalCourier.find(params[:with_courier][:internal_courier_id])
      params[:with_courier][:courier_name] = internal_couriers.name
      if @with_courier.update_attributes(with_courier_params)
        id_inbounds = params[:inbounds]
        id_inbounds.each do |id|
          inbound = Inbound.find(id)
          inbound.with_courier_id = @with_courier.id
          inbound.with_courier_date = @with_courier.with_courier_date
          inbound.with_courier_time = @with_courier.with_courier_time
          inbound.inbound_status = 2
          inbound.save
        end 
        format.html { redirect_to(with_courier_path(@with_courier), :notice => 'Inbound was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @with_courier.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @with_courier = WithCourier.find(params[:id])
    @with_courier.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'inbounds') }
      format.xml  { head :ok }
    end
  end

  private
  def with_courier_params
    params.require(:with_courier).permit(:with_courier_date, :with_courier_time, :courier_name, :internal_courier_id, :building_id)
  end
end