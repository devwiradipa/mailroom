class FailedsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

  def index
    if current_user.building
      @faileds = Failed.where("building_id = ?", current_user.building_id).order("created_at DESC")
    else
      @faileds = Failed.order("created_at DESC")
    end
  end

  def new
    @failed = Failed.new
    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 2, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 2)
    end
      
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @inbounds }
    end
  end

  def show
    @inbound = Inbound.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @inbounds }
    end
  end
  
  def create
    @failed = Failed.new(failed_params)
    internal_couriers = InternalCourier.find(params[:failed][:internal_courier_id])
    @failed.courier_name = internal_couriers.name

    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 2, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 2)
    end

    respond_to do |format|
      if @failed.save 
        id_inbounds = params[:inbounds]
        id_inbounds.each do |id|
          inbound = Inbound.find(id)
          inbound.failed_id = @failed.id
          inbound.failed_date = @failed.failed_date
          inbound.failed_time = @failed.failed_time
          inbound.inbound_status = 4
          inbound.save
        end

        format.html { redirect_to(faileds_path, :notice => 'Failed was successfully created.') }
        format.xml  { render :xml => @failed, :status => :created, :failed => @failed }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @failed.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @inbound = Inbound.find(params[:id])
      @companies = Company.all
      @departments = Department.all
      @users = User.all
      @couriers = Courier.all
      @package_types = PackageType.all

      @inbound_number = @inbound.inbound_number
  end
  
  def update
    @inbound = Inbound.find(params[:id])
    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 2, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 2)
    end

    @inbound_number = @inbound.inbound_number

    respond_to do |format|
      if @inbound.update_attributes(failed_params)        
        format.html { redirect_to(inbound_path(@inbound), :notice => 'Inbound was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @inbound.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @inbound = Inbound.find(params[:id])
    @inbound.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'inbounds') }
      format.xml  { head :ok }
    end
  end

  private
  def failed_params
    params.require(:failed).permit(:failed_date, :failed_time, :reason, :courier_name, :internal_courier_id, :building_id)
  end
end
