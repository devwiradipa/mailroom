class DeliveredsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

  def index
    if current_user.building
      @delivereds = Delivered.where("building_id = ?", current_user.building_id).order("created_at DESC")
    else
      @delivereds = Delivered.order("created_at DESC")
    end
  end

  def new
    @delivered = Delivered.new
    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 2, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 2)
    end
      
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @inbounds }
    end
  end

  def show
    @delivered = Delivered.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @inbounds }
    end
  end
  
  def create
    @delivered = Delivered.new(delivered_params)
    internal_couriers = InternalCourier.find(params[:delivered][:internal_courier_id])
    @delivered.courier_name = internal_couriers.name

    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 2, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 2)
    end

    respond_to do |format|
      if @delivered.save 
        id_inbounds = params[:inbounds]
        id_inbounds.each do |id|
          inbound = Inbound.find(id)
          inbound.delivered_id = @delivered.id
          inbound.delivered_date = @delivered.delivered_date
          inbound.delivered_time = @delivered.delivered_time
          inbound.inbound_status = 3
          inbound.save
        end

        format.html { redirect_to(delivereds_path, :notice => 'Delivered was successfully created.') }
        format.xml  { render :xml => @delivered, :status => :created, :delivered => @delivered }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @delivered.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @inbound = Inbound.find(params[:id])
    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 2, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 2)
    end

      @inbound_number = @inbound.inbound_number
  end
  
  def update
    @inbound = Inbound.find(params[:id])
    if current_user.building
      @internal_couriers = current_user.building.internal_couriers
      @inbounds = Inbound.where(inbound_status: 2, building_id: current_user.building_id)
    else
      @internal_couriers = InternalCourier.all
      @inbounds = Inbound.where(inbound_status: 2)
    end

    @inbound_number = @inbound.inbound_number

    respond_to do |format|
      if @inbound.update_attributes(delivered_params)        
        format.html { redirect_to(inbound_path(@inbound), :notice => 'Inbound was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @inbound.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @inbound = Inbound.find(params[:id])
    @inbound.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'inbounds') }
      format.xml  { head :ok }
    end
  end

  private
  def delivered_params
    params.require(:delivered).permit(:delivered_date, :delivered_time, :receiver_name, :internal_courier_id, :building_id)
  end
end
