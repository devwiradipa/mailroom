class InboundsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

    def index
      if current_user.user_group.super == 1
        if current_user.building
          @inbounds = Inbound.where("building_id = ?", current_user.building_id).order("created_at DESC")
        else
          @inbounds = Inbound.order("created_at DESC")
        end
      else
        @inbounds = current_user.inbounds.order("created_at DESC")
      end
    end

    def new
      if current_user.building
        @inbound = Inbound.new

        @numbering_format = NumberingFormat.where("name = 'INB' and year = ? and building_id = ?", Time.now.year, current_user.building_id).first

        if ! @numbering_format
          @numbering_format = NumberingFormat.new
          @numbering_format.name = "INB"
          @numbering_format.year = Time.now.year
          @numbering_format.iteration_number = 1
          @numbering_format.building_id = current_user.building_id
          @numbering_format.save
        end

        @inbound_number = "INB/"+Time.now.strftime("%Y/%m/%d/")+(sprintf "%06d", @numbering_format.iteration_number).to_s

        @companies = current_user.building.companies
        @departments = current_user.building.departments
        @users = current_user.building.users
        @couriers = current_user.building.couriers
        @package_types = PackageType.all
        @urgent_levels = UrgentLevel.all
        
        respond_to do |format|
          format.html # new.html.erb
          format.xml  { render :xml => @inbounds }
        end
      else
        @inbound = Inbound.new

        @numbering_format = NumberingFormat.where("name = 'INB' and year = ?", Time.now.year).first

        if ! @numbering_format
          @numbering_format = NumberingFormat.new
          @numbering_format.name = "INB"
          @numbering_format.year = Time.now.year
          @numbering_format.iteration_number = 1
          @numbering_format.save
        end

        @inbound_number = "INB/"+Time.now.strftime("%Y/%m/%d/")+(sprintf "%06d", @numbering_format.iteration_number).to_s

        @companies = Company.all
        @departments = Department.all
        @users = User.all
        @couriers = Courier.all
        @package_types = PackageType.all
        @urgent_levels = UrgentLevel.all
        
        respond_to do |format|
          format.html # new.html.erb
          format.xml  { render :xml => @inbounds }
        end
      end
    end

    def show
    @inbound = Inbound.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @inbounds }
    end
  end
  
  def create
    if current_user.building
      @inbound = Inbound.new(inbound_params)
      @numbering_format = NumberingFormat.where("name = 'INB' and year = ? and building_id = ?", Time.now.year, current_user.building_id).first
      @companies = current_user.building.companies
      @departments = current_user.building.departments
      @users = current_user.building.users
      @couriers = current_user.building.couriers
      @package_types = PackageType.all
      @urgent_levels = UrgentLevel.all
      
      if ! @inbound.courier_id || @inbound.courier_id == ""
        courier = Courier.where(name: params[:new_courier]).first
        if ! courier
          courier = Courier.new
          courier.name = params[:new_courier]
          if current_user.building_id
            courier.building_id = current_user.building_id
          end
          courier.save
        end

        @inbound.courier_id = courier.id
      end

      @inbound_number = "INB/"+Time.now.strftime("%Y/%m/%d/")+(sprintf "%06d", @numbering_format.iteration_number).to_s

      respond_to do |format|
        if @inbound.save 

          # UserMailer.inbound_notification(@inbound.id, @inbound.package_type.name, @inbound.description, @inbound.created.name, @inbound.user.name, @inbound.user.email, @inbound.sender_name, @inbound.sender_department, @inbound.sender_company, @hostname).deliver
          @numbering_format.update_attributes(iteration_number: @numbering_format.iteration_number+1)
          @package_status = PackageStatus.where(ordering_number: 1).first

          @inbound_status = InboundStatus.new
          @inbound_status.inbound_id = @inbound.id
          @inbound_status.package_status_id = @package_status.id
          @inbound_status.created_by = current_user.id
          @inbound_status.save

          format.html { redirect_to(inbound_path(@inbound.id), :notice => 'Inbound was successfully created.') }
          format.xml  { render :xml => @inbound, :status => :created, :inbound => @inbound }
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @inbound.errors, :status => :unprocessable_entity }
        end
      end
    else
      @inbound = Inbound.new(inbound_params)
      @numbering_format = NumberingFormat.where("name = 'INB' and year = ?", Time.now.year).first
      @companies = Company.all
      @departments = Department.all
      @users = User.all
      @couriers = Courier.all
      @package_types = PackageType.all
      @urgent_levels = UrgentLevel.all
      
      if ! @inbound.courier_id || @inbound.courier_id == ""
        courier = Courier.where(name: params[:new_courier]).first
        if ! courier
          courier = Courier.new
          courier.name = params[:new_courier]
          if current_user.building_id
            courier.building_id = current_user.building_id
          end
          courier.save
        end

        @inbound.courier_id = courier.id
      end

      @inbound_number = "INB/"+Time.now.strftime("%Y/%m/%d/")+(sprintf "%06d", @numbering_format.iteration_number).to_s

      respond_to do |format|
        if @inbound.save 

          if @inbound.user
            UserMailer.inbound_notification(@inbound.id, @inbound.package_type.name, @inbound.description, @inbound.created.name, @inbound.user.name, @inbound.user.email, @inbound.sender_name, @inbound.sender_department, @inbound.sender_company, @hostname).deliver
          end
          @numbering_format.update_attributes(iteration_number: @numbering_format.iteration_number+1)
          @package_status = PackageStatus.where(ordering_number: 1).first

          @inbound_status = InboundStatus.new
          @inbound_status.inbound_id = @inbound.id
          @inbound_status.package_status_id = @package_status.id
          @inbound_status.created_by = current_user.id
          @inbound_status.save

          format.html { redirect_to(inbound_path(@inbound.id), :notice => 'Inbound was successfully created.') }
          format.xml  { render :xml => @inbound, :status => :created, :inbound => @inbound }
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @inbound.errors, :status => :unprocessable_entity }
        end
      end
    end
  end
  
  def edit
    if current_user.building
      @inbound = Inbound.find(params[:id])
      @companies = current_user.building.companies
      @departments = current_user.building.departments
      @users = current_user.building.users
      @couriers = current_user.building.couriers
      @package_types = PackageType.all
      @urgent_levels = UrgentLevel.all

      @inbound_number = @inbound.inbound_number
    else
      @inbound = Inbound.find(params[:id])
      @companies = Company.all
      @departments = Department.all
      @users = User.all
      @couriers = Courier.all
      @package_types = PackageType.all
      @urgent_levels = UrgentLevel.all

      @inbound_number = @inbound.inbound_number
    end
  end
  
  def update
    if current_user.building
      @inbound = Inbound.find(params[:id])
      @companies = current_user.building.companies
      @departments = current_user.building.departments
      @users = current_user.building.users
      @couriers = current_user.building.couriers
      @package_types = PackageType.all
      @urgent_levels = UrgentLevel.all

      @inbound_number = @inbound.inbound_number
    else
      @inbound = Inbound.find(params[:id])
      @companies = Company.all
      @departments = Department.all
      @users = User.all
      @couriers = Courier.all
      @package_types = PackageType.all
      @urgent_levels = UrgentLevel.all

      @inbound_number = @inbound.inbound_number
    end

    respond_to do |format|
      if @inbound.update_attributes(inbound_params)      

        if params[:new_courier] && params[:new_courier] != ""
          @courier = Courier.new
          @courier.name = params[:new_courier]
          @courier.save
          @inbound.update_attributes(courier_id: @courier.id)
        end  
        format.html { redirect_to(inbound_path(@inbound), :notice => 'Inbound was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @inbound.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @inbound = Inbound.find(params[:id])
    @inbound.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'inbounds') }
      format.xml  { head :ok }
    end
  end
  
  def forward
    @inbound = Inbound.find(params[:id])
    @inbound.update_attributes(inbound_status: 2)
    @package_status = PackageStatus.where(ordering_number: 2).first
    @inbound_status = InboundStatus.new
    @inbound_status.inbound_id = @inbound.id
    @inbound_status.package_status_id = @package_status.id
    @inbound_status.created_by = current_user.id
    @inbound_status.save

    respond_to do |format|
      format.html { redirect_to(:controller => 'inbounds') }
      format.xml  { head :ok }
    end
  end
  
  def receive
    @inbound = Inbound.find(params[:id])
    @inbound.update_attributes(inbound_status: 3)
    @package_status = PackageStatus.where(ordering_number: 3).first
    @inbound_status = InboundStatus.new
    @inbound_status.inbound_id = @inbound.id
    @inbound_status.package_status_id = @package_status.id
    @inbound_status.created_by = current_user.id
    @inbound_status.save

    respond_to do |format|
      format.html { redirect_to(:controller => 'inbounds') }
      format.xml  { head :ok }
    end
  end

  def get_inbounds
    if current_user.building
      this_result = []
      last_result = []

      (1..12).each do |m|
        if current_user.user_group.super == 1
          @this_inbounds = Inbound.where("MONTH(created_at) = ? AND YEAR(created_at) = ? AND building_id = ?", m, Time.now.year, current_user.building_id)
          @last_inbounds = Inbound.where("MONTH(created_at) = ? AND YEAR(created_at) = ? AND building_id = ?", m, Time.now.year-1, current_user.building_id)
        else
          @this_inbounds = current_user.inbounds.where("MONTH(created_at) = ? AND YEAR(created_at) = ?", m, Time.now.year)
          @last_inbounds = current_user.inbounds.where("MONTH(created_at) = ? AND YEAR(created_at) = ?", m, Time.now.year-1)
        end

        this_result << @this_inbounds.count
        last_result << @last_inbounds.count
      end

      json_result = Hash.new
      json_result[:this] = this_result
      json_result[:last] = last_result

      render :json => json_result, :layout => false
    else
      this_result = []
      last_result = []

      (1..12).each do |m|
        if current_user.user_group.super == 1
          @this_inbounds = Inbound.where("MONTH(created_at) = ? AND YEAR(created_at) = ?", m, Time.now.year)
          @last_inbounds = Inbound.where("MONTH(created_at) = ? AND YEAR(created_at) = ?", m, Time.now.year-1)
        else
          @this_inbounds = current_user.inbounds.where("MONTH(created_at) = ? AND YEAR(created_at) = ?", m, Time.now.year)
          @last_inbounds = current_user.inbounds.where("MONTH(created_at) = ? AND YEAR(created_at) = ?", m, Time.now.year-1)
        end

        this_result << @this_inbounds.count
        last_result << @last_inbounds.count
      end

      json_result = Hash.new
      json_result[:this] = this_result
      json_result[:last] = last_result

      render :json => json_result, :layout => false
    end
  end
  
  private
  
  def inbound_params
    params.require(:inbound).permit(:created_by, :description, :name, :updated_by, :company_id, :department_id, :user_id, :inbound_number, :inbound_date, :inbound_time, :awb_number, :courier_id, :awb_date, :sender_name, :sender_department, :sender_company, :package_type_id, :inbound_status, :urgent_level_id, :building_id)
  end
end
