class InboundStatusesController < ApplicationController
  before_filter :authorize_user
  
  private
  
  def inbound_status_params
    params.require(:inbound_status).permit(:created_by, :inbound_id, :package_status_id, :updated_by, :ordering_number)
  end
end
