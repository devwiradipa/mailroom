class PackageStatusesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user
  before_filter :authorize_super, except: [:index]

    def index
      @package_statuses = PackageStatus.order("created_at DESC")
    end

    def new
      @package_status = PackageStatus.new
      
      respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @package_statuses }
      end
    end

    def show
    @package_status = PackageStatus.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @package_statuses }
    end
  end
  
  def create
    @package_status = PackageStatus.new(package_status_params)

    respond_to do |format|
      if @package_status.save        
        format.html { redirect_to(package_status_path(@package_status.id), :notice => 'PackageStatus was successfully created.') }
        format.xml  { render :xml => @package_status, :status => :created, :package_status => @package_status }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @package_status.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @package_status = PackageStatus.find(params[:id])
  end
  
  def update
    @package_status = PackageStatus.find(params[:id])

    respond_to do |format|
      if @package_status.update_attributes(package_status_params)        
        format.html { redirect_to(package_status_path(@package_status), :notice => 'PackageStatus was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @package_status.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @package_status = PackageStatus.find(params[:id])
    @package_status.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'package_statuses') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def package_status_params
    params.require(:package_status).permit(:created_by, :description, :name, :updated_by, :ordering_number)
  end
end
