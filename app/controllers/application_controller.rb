class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :layout_by_resource
  
  before_filter :controller_model
  
  protected

  def controller_model
    @months = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Jul", "Agustus", "September", "Oktober", "November", "Desember"]
    @days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"]
    @now = Time.now
    @hostname = "http://mailroom.wiradipa.com"
  end
  
  def layout_by_resource
    if devise_controller?
      "user_login"
    else
      "application"
    end
  end
  
  def after_sign_in_path_for(resource_or_scope)
    root_path
  end
  
  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  def authorize_super
    if current_user.building
      redirect_to(:controller => "homes")
    end
  end

  def authorize_user
    @not_authorized = true
    current_user.user_group.action_models.each do |action_model|
      if params[:controller] == action_model.controller_model.path && params[:action] == action_model.path
        @not_authorized = false
      end
      
      break if ! @not_authorized
    end
    
    @action_model = ActionModel.where("path = ?", params[:action]).first
    
    if ! @action_model
      @not_authorized = false
    end
    
    if current_user.user_group && current_user.user_group.super == 1
      @not_authorized = false
    end
    
    if @not_authorized
      redirect_to(:controller => "homes")
    end
  end
end
