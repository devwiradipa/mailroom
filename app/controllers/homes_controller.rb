class HomesController < ApplicationController
	before_filter :authenticate_user!

	def index
		if current_user.building
			@package_types = PackageType.all
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = current_user.building.inbounds
			@unprocessed = Inbound.where("inbound_status = ? AND building_id = ?", @in_mailroom.id, current_user.building_id)
			@unprocessed_under_2_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2", @in_mailroom.id, current_user.building_id)
			@unprocessed_2_5_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5", @in_mailroom.id, current_user.building_id)
			@unprocessed_over_5_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5", @in_mailroom.id, current_user.building_id)

			@delivered = Inbound.where("inbound_status = ? AND building_id = ?", @sent.id, current_user.building_id)
			@delivered_under_2_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2", @sent.id, current_user.building_id)
			@delivered_2_5_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5", @sent.id, current_user.building_id)
			@delivered_over_5_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5", @sent.id, current_user.building_id)

			@this_month_unproc = 0
			@this_month_proc_under_2_days = 0
			@this_month_proc_2_5_days = 0
			@this_month_proc_over_5_days = 0
		else
			@package_types = PackageType.all
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.all
			@unprocessed = Inbound.where("inbound_status = ?", @in_mailroom.id)
			@unprocessed_under_2_days = Inbound.where("inbound_status = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2", @in_mailroom.id)
			@unprocessed_2_5_days = Inbound.where("inbound_status = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5", @in_mailroom.id)
			@unprocessed_over_5_days = Inbound.where("inbound_status = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5", @in_mailroom.id)

			@delivered = Inbound.where("inbound_status = ?", @sent.id)
			@delivered_under_2_days = Inbound.where("inbound_status = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2", @sent.id)
			@delivered_2_5_days = Inbound.where("inbound_status = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5", @sent.id)
			@delivered_over_5_days = Inbound.where("inbound_status = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5", @sent.id)

			@this_month_unproc = 0
			@this_month_proc_under_2_days = 0
			@this_month_proc_2_5_days = 0
			@this_month_proc_over_5_days = 0
		end
	end

	def get_monthly_statistic
		if current_user.building
			statistics = Hash.new
			yearmonth = params[:yearmonth]
			unprocessed = Inbound.where("inbound_status = ? AND building_id = ? and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 1, current_user.building_id, yearmonth).count
			less_two_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2 and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 3, current_user.building_id, yearmonth).count
			two_to_five = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5 and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 3, current_user.building_id, yearmonth).count
			more_five_days = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5 and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 3, current_user.building_id, yearmonth).count
			
			statistics[:unprocessed] = unprocessed
			statistics[:less_two_days] = less_two_days
			statistics[:two_to_five] = two_to_five
			statistics[:more_five_days] = more_five_days
			render json:statistics
		else
			statistics = Hash.new
			yearmonth = params[:yearmonth]
			unprocessed = Inbound.where("inbound_status = ? and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 1, yearmonth).count
			less_two_days = Inbound.where("inbound_status = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2 and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 3, yearmonth).count
			two_to_five = Inbound.where("inbound_status = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5 and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 3, yearmonth).count
			more_five_days = Inbound.where("inbound_status = ? AND DATEDIFF(TIMESTAMP(inbounds.delivered_date, inbounds.delivered_time),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5 and EXTRACT(YEAR_MONTH FROM TIMESTAMP(inbounds.inbound_date)) = ? ", 3, yearmonth).count
			
			statistics[:unprocessed] = unprocessed
			statistics[:less_two_days] = less_two_days
			statistics[:two_to_five] = two_to_five
			statistics[:more_five_days] = more_five_days
			render json:statistics
		end
	end

	def unprocessed_under_2_days
		if current_user.building
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2", @in_mailroom.id, current_user.building_id)
		else
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) < 2", @in_mailroom.id)
		end
	end

	def unprocessed_2_5_days
		if current_user.building
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5", @in_mailroom.id, current_user.building_id)
		else
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) >= 2 AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) <= 5", @in_mailroom.id)
		end
	end

	def unprocessed
		if current_user.building
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ? AND building_id = ?", @in_mailroom.id, current_user.building_id)
		else
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ?", @in_mailroom.id)
		end
	end

	def unprocessed_over_5_days
		if current_user.building
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ? AND building_id = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5", @in_mailroom.id, current_user.building_id)
		else
			@in_mailroom = PackageStatus.where("ordering_number = 1").first
			@sent = PackageStatus.where("ordering_number = 3").first

			@inbounds = Inbound.where("inbound_status = ? AND DATEDIFF(NOW(),TIMESTAMP(inbounds.inbound_date, inbounds.inbound_time)) > 5", @in_mailroom.id)
		end
	end
end
