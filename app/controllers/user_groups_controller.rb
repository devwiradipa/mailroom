class UserGroupsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

  def index
  	 if current_user.building
        @user_groups = UserGroup.where("building_id = ?", current_user.building_id).order("created_at DESC")
      else
        @user_groups = UserGroup.order("created_at DESC")
      end
  end

  def new
  	@user_group = UserGroup.new
    @controller_models = ControllerModel.all
    @action_models = ActionModel.order("controller_model_id ASC")
    @buildings = Building.order("name ASC")
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user_group }
    end  	
  end

  def show
    @user_group = UserGroup.find(params[:id])
    @action_models = @user_group.action_models.order("controller_model_id ASC")
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user_group }
    end
  end
  
  def create
    @user_group = UserGroup.new(user_group_params)
    @controller_models = ControllerModel.all
    @action_models = ActionModel.order("controller_model_id ASC")
    @buildings = Building.order("name ASC")

    respond_to do |format|
      if @user_group.save

        format.html { redirect_to(user_group_path(@user_group.id), :notice => 'User Group was successfully created.') }
        format.xml  { render :xml => @user_group, :status => :created, :user_group => @user_group }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user_group.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @user_group = UserGroup.find(params[:id])
    @controller_models = ControllerModel.all
    @action_models = ActionModel.order("controller_model_id ASC")
    @buildings = Building.order("name ASC")
  end
  
  def update
    @user_group = UserGroup.find(params[:id])
    @controller_models = ControllerModel.all
    @action_models = ActionModel.order("controller_model_id ASC")
    @buildings = Building.order("name ASC")

    respond_to do |format|
      if @user_group.update_attributes(user_group_params)  
        format.html { redirect_to(user_group_path(@user_group.id), :notice => 'User Group was successfully updated.') }
      
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user_group.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @user_group = UserGroup.find(params[:id])
    @user_group.destroy

     respond_to do |format|
      format.html { redirect_to(:controller => 'user_groups') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def user_group_params
    params.require(:user_group).permit(:created_by, :description, :name, :updated_by, :super, :building_id, action_model_ids: [])
  end
end
