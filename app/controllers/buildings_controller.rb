class BuildingsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

    def index
      @buildings = Building.order("created_at DESC")
    end

    def new
      @building = Building.new
      
      respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @buildings }
      end
    end

    def show
    @building = Building.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @buildings }
    end
  end
  
  def create
    @building = Building.new(building_params)

    respond_to do |format|
      if @building.save        
        format.html { redirect_to(building_path(@building.id), :notice => 'Building was successfully created.') }
        format.xml  { render :xml => @building, :status => :created, :building => @building }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @building.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @building = Building.find(params[:id])
  end
  
  def update
    @building = Building.find(params[:id])

    respond_to do |format|
      if @building.update_attributes(building_params)        
        format.html { redirect_to(building_path(@building), :notice => 'Building was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @building.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @building = Building.find(params[:id])
    @building.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'buildings') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def building_params
    params.require(:building).permit(:created_by, :description, :name, :updated_by)
  end
end
