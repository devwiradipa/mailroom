class DepartmentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

    def index
      if current_user.building
        @departments = Department.where("building_id = ?", current_user.building_id).order("created_at DESC")
      else
        @departments = Department.order("created_at DESC")
      end
    end

  def new
    @department = Department.new
      if current_user.building
      @companies = current_user.building.companies
    else
      @companies = Company.all
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @departments }
    end
  end

  def show
    @department = Department.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @departments }
    end
  end
  
  def create
    @department = Department.new(department_params)
    if current_user.building
      @companies = current_user.building.companies
    else
      @companies = Company.all
    end

    respond_to do |format|
      if @department.save        
        format.html { redirect_to(department_path(@department.id), :notice => 'Department was successfully created.') }
        format.xml  { render :xml => @department, :status => :created, :department => @department }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @department.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @department = Department.find(params[:id])
    if current_user.building
      @companies = current_user.building.companies
    else
      @companies = Company.all
    end
  end
  
  def update
    @department = Department.find(params[:id])
    if current_user.building
      @companies = current_user.building.companies
    else
      @companies = Company.all
    end

    respond_to do |format|
      if @department.update_attributes(department_params)        
        format.html { redirect_to(department_path(@department), :notice => 'Department was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @department.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @department = Department.find(params[:id])
    @department.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'departments') }
      format.xml  { head :ok }
    end
  end

  def get_departments
    @company = Company.find(params[:company_id].to_i)
    @departments = @company.departments

    respond_to do |format|
      format.js {
        render :json => @departments.to_json(:except => [:updated_at]),
        :callback => params[:callback]
      }
    end
  end
  
  private
  
  def department_params
    params.require(:department).permit(:created_by, :description, :name, :updated_by, :company_id, :building_id)
  end
end
