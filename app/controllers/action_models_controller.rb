class ActionModelsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user
  before_filter :authorize_super, except: [:index]
  
  def new
    @action_model = ActionModel.new
    if params[:controller_model_id]
      @controller_model = ControllerModel.find(params[:controller_model_id].to_i)
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @action_model }
    end
  end
  
  def create
    @action_model = ActionModel.new(action_model_params)
    if params[:action_model][:controller_model_id]
      @controller_model = ControllerModel.find(params[:action_model][:controller_model_id].to_i)
    end

    respond_to do |format|
      if @action_model.save
        
        format.html { redirect_to(controller_model_path(@action_model.controller_model_id), :notice => 'ActionModel was successfully created.') }
        format.xml  { render :xml => @action_model, :status => :created, :action_model => @action_model }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @action_model.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @action_model = ActionModel.find(params[:id])
    @controller_model = @action_model.controller_model
  end
  
  def update
    @action_model = ActionModel.find(params[:id])
    @controller_model = @action_model.controller_model

    respond_to do |format|
      if @action_model.update_attributes(action_model_params)
        
        format.html { redirect_to(controller_model_path(@action_model.controller_model_id), :notice => 'ActionModel was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @action_model.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @action_model = ActionModel.find(params[:id])
    @action_model.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.xml  { head :ok }
    end
  end

  private

  def action_model_params
    params.require(:action_model).permit(:controller_model_id, :created_by, :description, :name, :path, :updated_by)
  end
end
