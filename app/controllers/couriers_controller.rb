class CouriersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

    def index
      if current_user.building
        @couriers = Courier.where("building_id = ?", current_user.building_id).order("created_at DESC")
      else
        @couriers = Courier.order("created_at DESC")
      end
    end

    def new
      @courier = Courier.new
      
      respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @couriers }
      end
    end

    def show
    @courier = Courier.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @couriers }
    end
  end
  
  def create
    @courier = Courier.new(courier_params)

    respond_to do |format|
      if @courier.save        
        format.html { redirect_to(courier_path(@courier.id), :notice => 'Courier was successfully created.') }
        format.xml  { render :xml => @courier, :status => :created, :courier => @courier }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @courier.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @courier = Courier.find(params[:id])
  end
  
  def update
    @courier = Courier.find(params[:id])

    respond_to do |format|
      if @courier.update_attributes(courier_params)        
        format.html { redirect_to(courier_path(@courier), :notice => 'Courier was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @courier.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @courier = Courier.find(params[:id])
    @courier.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'couriers') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def courier_params
    params.require(:courier).permit(:created_by, :description, :name, :updated_by, :building_id)
  end
end
