class PackageTypesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user
  before_filter :authorize_super, except: [:index]

  def index
    @package_types = PackageType.order("created_at DESC")
  end

  def new
    @package_type = PackageType.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @package_types }
    end
  end

  def show
    @package_type = PackageType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @package_types }
    end
  end
  
  def create
    @package_type = PackageType.new(package_type_params)

    respond_to do |format|
      if @package_type.save        
        format.html { redirect_to(package_type_path(@package_type.id), :notice => 'PackageType was successfully created.') }
        format.xml  { render :xml => @package_type, :status => :created, :package_type => @package_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @package_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @package_type = PackageType.find(params[:id])
  end
  
  def update
    @package_type = PackageType.find(params[:id])

    respond_to do |format|
      if @package_type.update_attributes(package_type_params)        
        format.html { redirect_to(package_type_path(@package_type), :notice => 'PackageType was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @package_type.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @package_type = PackageType.find(params[:id])
    @package_type.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'package_types') }
      format.xml  { head :ok }
    end
  end

  def get_package_types
    @package_types = PackageType.all

    json_result = []

    if current_user.user_group.super == 1
      @package_types.each do |package_type|
        json_data = Hash.new
        json_data[:value] = package_type.inbounds.count
        json_data[:color] = package_type.color
        json_data[:highlight] = package_type.highlight
        json_data[:label] = package_type.name

        json_result << json_data
      end
    else
      @package_types.each do |package_type|
        json_data = Hash.new
        json_data[:value] = package_type.inbounds.where("user_id = ?", current_user.id).count
        json_data[:color] = package_type.color
        json_data[:highlight] = package_type.highlight
        json_data[:label] = package_type.name

        json_result << json_data
      end
    end

    render :json => json_result, :layout => false
  end
  
  private
  
  def package_type_params
    params.require(:package_type).permit(:created_by, :description, :name, :color, :highlight)
  end
end
