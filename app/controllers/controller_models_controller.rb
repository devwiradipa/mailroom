class ControllerModelsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user
  before_filter :authorize_super, except: [:index]
  
  def index
    @controller_models = ControllerModel.order("created_at DESC")
  end
  
  def new
    @controller_model = ControllerModel.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @controller_model }
    end
  end
  
  def show
    @controller_model = ControllerModel.find(params[:id])
    @action_models = @controller_model.action_models

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @branch }
    end
  end
  
  def create
    @controller_model = ControllerModel.new(controller_model_params)

    respond_to do |format|
      if @controller_model.save
        @index = ActionModel.new
        @index.name = "Halaman Utama"
        @index.path = "index"
        @index.controller_model_id = @controller_model.id
        @index.save
        
        @new = ActionModel.new
        @new.name = "Tambah"
        @new.path = "new"
        @new.controller_model_id = @controller_model.id
        @new.save
        
        @edit = ActionModel.new
        @edit.name = "Ubah"
        @edit.path = "edit"
        @edit.controller_model_id = @controller_model.id
        @edit.save
        
        @show = ActionModel.new
        @show.name = "Lihat Detil"
        @show.path = "show"
        @show.controller_model_id = @controller_model.id
        @show.save
        
        @destroy = ActionModel.new
        @destroy.name = "Hapus"
        @destroy.path = "destroy"
        @destroy.controller_model_id = @controller_model.id
        @destroy.save
        
        format.html { redirect_to(controller_model_path(@controller_model.id), :notice => 'ControllerModel was successfully created.') }
        format.xml  { render :xml => @controller_model, :status => :created, :controller_model => @controller_model }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @controller_model.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @controller_model = ControllerModel.find(params[:id])
  end
  
  def update
    @controller_model = ControllerModel.find(params[:id])

    respond_to do |format|
      if @controller_model.update_attributes(controller_model_params)
        
        format.html { redirect_to(controller_model_path(@controller_model), :notice => 'ControllerModel was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @controller_model.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @controller_model = ControllerModel.find(params[:id])
    @controller_model.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'controller_models') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def controller_model_params
    params.require(:controller_model).permit(:category, :controller_model_group, :created_by, :description, :icon, :name, :order_number, :path, :position, :updated_by)
  end
end
