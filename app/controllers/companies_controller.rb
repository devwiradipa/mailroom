class CompaniesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

    def index
      if current_user.building
        @companies = Company.where("building_id = ?", current_user.building_id).order("created_at DESC")
      else
        @companies = Company.order("created_at DESC")
      end
    end

    def new
      @company = Company.new
      
      respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @companies }
      end
    end

    def show
    @company = Company.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @companies }
    end
  end
  
  def create
    @company = Company.new(company_params)

    respond_to do |format|
      if @company.save        
        format.html { redirect_to(company_path(@company.id), :notice => 'Company was successfully created.') }
        format.xml  { render :xml => @company, :status => :created, :company => @company }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @company.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @company = Company.find(params[:id])
  end
  
  def update
    @company = Company.find(params[:id])

    respond_to do |format|
      if @company.update_attributes(company_params)        
        format.html { redirect_to(company_path(@company), :notice => 'Company was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @company.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @company = Company.find(params[:id])
    @company.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'companies') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def company_params
    params.require(:company).permit(:created_by, :description, :name, :updated_by, :building_id)
  end
end
