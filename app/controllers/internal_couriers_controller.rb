class InternalCouriersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authorize_user

  def index
    if current_user.building
      @internal_couriers = InternalCourier.where("building_id = ?", current_user.building_id).order("created_at DESC")
    else
      @internal_couriers = InternalCourier.order("created_at DESC")
    end
  end

  def new
    @internal_courier = InternalCourier.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @internal_couriers }
    end
  end

  def show
    @internal_courier = InternalCourier.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @internal_couriers }
    end
  end
  
  def create
    @internal_courier = InternalCourier.new(internal_courier_params)

    respond_to do |format|
      if @internal_courier.save        
        format.html { redirect_to(internal_courier_path(@internal_courier.id), :notice => 'InternalCourier was successfully created.') }
        format.xml  { render :xml => @internal_courier, :status => :created, :internal_courier => @internal_courier }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @internal_courier.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def edit
    @internal_courier = InternalCourier.find(params[:id])
  end
  
  def update
    @internal_courier = InternalCourier.find(params[:id])

    respond_to do |format|
      if @internal_courier.update_attributes(internal_courier_params)        
        format.html { redirect_to(internal_courier_path(@internal_courier), :notice => 'InternalCourier was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @internal_courier.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @internal_courier = InternalCourier.find(params[:id])
    @internal_courier.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'internal_couriers') }
      format.xml  { head :ok }
    end
  end
  
  private
  
  def internal_courier_params
    params.require(:internal_courier).permit(:created_by, :description, :name, :address, :updated_by, :building_id)
  end
end
