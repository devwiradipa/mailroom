Rails.application.routes.draw do
  devise_for :users, controllers: {
        sessions: 'users/sessions'
      }

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'homes#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'
  post "json/departments/get_departments" => "departments#get_departments"
  post "json/users/get_users" => "users#get_users"
  post "json/package_types/get_package_types" => "package_types#get_package_types"
  post "json/inbounds/get_inbounds" => "inbounds#get_inbounds"

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase
  get 'get_monthly_statistic' => "homes#get_monthly_statistic"

  # Example resource route (maps HTTP verbs to controller actions automatically):
    resources :homes do 
      collection do
        get :unprocessed
        get :unprocessed_under_2_days
        get :unprocessed_2_5_days
        get :unprocessed_over_5_days
      end
    end
    resources :outbonds
    resources :inbounds do
      member do
        get :forward
        get :receive
      end
    end
    resources :internal_couriers
    resources :urgent_levels
    resources :with_couriers
    resources :delivereds
    resources :faileds
    resources :couriers
    resources :companies
    resources :users
    resources :user_groups
    resources :departments
    resources :controller_models
    resources :action_models
    resources :package_statuses
    resources :package_types
    resources :inbound_statuses
    resources :buildings
end
